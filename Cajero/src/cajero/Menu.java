package cajero;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author josemurillo
 */
import java.util.Scanner;
import java.util.InputMismatchException;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public class Menu {
   private String operacion;
   private String dato;
   private String dato2;
   private final Scanner entrada = new Scanner(System.in);
   private ArrayList<Cliente>clientes = new ArrayList();
   
   
        
void mostrarMenuPrincipal(){
    System.out.println("Bienvenido al Banco Ten Forward");
    System.out.println("1. Ingreso administrador");
    System.out.println("2. Ingreso cliente");
    System.out.println("3.Salir");
    System.out.print("Seleccione una opcion: ");
    operacion = entrada.next();
    
    switch(operacion){
        case "1":
            try{
                File archivo = new File("PASSWORD.txt");
                FileReader leer = new FileReader(archivo);
                BufferedReader almacenamiento = new BufferedReader(leer);
                try{
                    String pass = almacenamiento.readLine();
                    try{
                        almacenamiento.close();
                        try{
                            leer.close();
                            System.out.print("Ingrese la clave del administrador: ");
                            dato = entrada.next();
                            if(pass.equalsIgnoreCase(dato)){
                                this.mostrarMenuAdmi();
                                break;
                            }else{
                                System.out.println("La password es invalida");
                                this.mostrarMenuPrincipal();
                                break;
                            }
                        }catch (IOException e){}
                    }catch (IOException e){}
                    
                }catch (IOException e){}
                
            }catch (FileNotFoundException e){}
        case "2":
            try{
                System.out.print("Ingrese su cedula: ");
                dato2 = entrada.next();
                System.out.print("Ingrese su clave: ");
                dato = entrada.next();
                System.out.println();
                if (dato.isEmpty() || dato2.isEmpty()){
                    System.out.println("Debe ingresar la clave y la cedula");
                    this.mostrarMenuPrincipal();
                    break;
                }else{
                    int posicion = -1;
                    for (int i = 0; i < clientes.size();i++){
                        if (clientes.get(i).getCedula().equalsIgnoreCase(dato2) && clientes.get(i).valiDarPassword(dato)){
                            posicion = i;
                            break;
                        }else{}
                    }
                    if (posicion == -1){
                        System.out.println("Ese cliente no esta registrado");
                        this.mostrarMenuPrincipal();
                    }else if(clientes.get(posicion).validar(dato, dato2)){
                        System.out.println("Bienvenido: "+clientes.get(posicion).getNombre());
                        this.mostrarMenuCliente(clientes.get(posicion));
                        break;
                    }else{
                    System.out.println("Clave incorrecta.");
                    this.mostrarMenuPrincipal();
                    break;
                    }
                }
            }catch(InputMismatchException e){
                System.out.println("No ingreso los datos solicitados");
                this.mostrarMenuPrincipal();
                break;
            }
            
        case "3":
            {
             this.salir();
            }
        default:
             System.out.println("Opcion no valida");
            this.mostrarMenuPrincipal();
}
    
}
        
    
private void mostrarMenuAdmi(){
    System.out.println("Menu de administracion");
    System.out.println("1.1 Crear cliente");
    System.out.println("1.2 Editar datos del cliente");
    System.out.println("1.3 Crear cuenta de cliente");
    System.out.println("1.4 Listado de clientes");
    System.out.println("1.5 Regresar");
    System.out.print("Seleccione una opcion: ");
    operacion = entrada.next();
    switch(operacion){
        case "1.1":
        {
            System.out.print("Ingrese el nombre del cliente nuevo: ");
            String nombre = entrada.next();
            System.out.print("Ingrese el numero de cedula del cliente: ");
            String cedula = entrada.next();
            System.out.print("Ingrese la clave para la cuenta: ");
            String clave = entrada.next();
            if (nombre.isEmpty()||cedula.isEmpty()||clave.isEmpty()){
                System.out.println("Debe ingresar todos los datos solicitados.");
                this.mostrarMenuAdmi();
            }else{
                  Cliente cliente = new Cliente(nombre,cedula,clave);
                  clientes.add(cliente);
                  System.out.println("La cuenta se ha creado.");
                  this.mostrarMenuAdmi(); 
                }
        break;
        }
        
        case "1.2":
        {
            if (clientes.isEmpty()){
                System.out.println("No hay ningun cliente.");
                this.mostrarMenuAdmi();
            }else{
                System.out.println("-----Listado de clientes-----");
                for(Cliente cliente:clientes){
                    System.out.println("Nombre: " + cliente.getNombre() + " |  Cedula: "+cliente.getCedula());
                }
                System.out.print("Ingrese la cedula del cliente; ");
                dato = entrada.next();
                if (dato.isEmpty()){
                    System.out.println("Debe ingresar una cedula.");
                    this.mostrarMenuAdmi();
                }else{
                    int posicion = -1;
                    for (int i = 0; i < clientes.size();i++){
                        if (dato.equalsIgnoreCase(clientes.get(i).getCedula())){
                            posicion = i;
                        }else{
                            posicion = posicion + 1;
                        }
                    }
                    if (posicion == -1){
                        System.out.println("La cedula no esta asociada con ningun cliente.");
                        this.mostrarMenuAdmi();
                    }else{
                        System.out.print("Ingrese el nombre que se va a asociar con la cuenta: ");
                        String nombre = entrada.next();
                        System.out.print("Ingrese la nueva cedula del cliente: ");
                        String cedula = entrada.next();
                        System.out.print("Ingrese la nueva clave: ");
                        String clave = entrada.next();
                        if (nombre.isEmpty()||cedula.isEmpty()||clave.isEmpty()){
                            System.out.println("Debe ingresar todos los datos solicitados.");
                            this.mostrarMenuAdmi();
                        
                        }else{
                            clientes.get(posicion).setCedula(cedula);
                            clientes.get(posicion).setNombre(nombre);
                            clientes.get(posicion).setPassword(clave);
                            System.out.println("La cuenta ha sido modificada con exito.");
                            this.mostrarMenuAdmi();                            
                            }
                        }
                    }
            }
        break;
        }
        
        case "1.3":
        {
            System.out.println("-----Listado de clientes-----");
            for(Cliente c:clientes){
                System.out.println("Nombre: " + c.getNombre() + "| Cedula: "+c.getCedula());
            }
            System.out.print("Ingrese la cedula del cliente; ");
            dato = entrada.next();
            if (dato.isEmpty()){
                System.out.println("Debe ingresar una cedula.");
                this.mostrarMenuAdmi();
            }else{
                int posicion = -1;
                for ( int i = 0;i < clientes.size();i++){
                    if (dato.equalsIgnoreCase(clientes.get(i).getCedula())){
                        posicion = i;
                        break;
                    }
                }
                if(posicion == -1){
                    System.out.println("No existe ningun cliente con esa cedula.");
                    this.mostrarMenuAdmi();
                }else if (clientes.get(posicion).getCedula().equalsIgnoreCase(dato)){
                    System.out.print("Ingrese el monto para esta nueva cuenta: ");
                    dato = entrada.next();
                    try{
                        double monto = Double.parseDouble(dato);
                        if (monto < 20000.0){
                            System.out.println("El minimo para abrir una cuenta es 20000.");
                            this.mostrarMenuAdmi();
                        }else{
                            if(clientes.get(posicion).getCantidadCuentas()==5){
                                System.out.println("Este cliente tiene el maximo numero de cuentas");
                                this.mostrarMenuAdmi();
                            }else{
                                try{
                                    clientes.get(posicion).crearCuenta(monto);
                                    System.out.println("La cuenta ha sido creada con exito.");
                                    this.mostrarMenuAdmi();
                                }catch (Exception e){
                                    System.out.println("Se presento un error al crear la cuenta.");
                                    this.mostrarMenuAdmi();
                                }
                            }
                        }
                    }catch(NumberFormatException e){
                        System.out.println("Debe ingresar un monto numerico.");
                        this.mostrarMenuAdmi();
                    }
                }
            }
        break;
        }
        
        case "1.4":
        {
            System.out.println("-----Listado de clientes-----");
            if(clientes.size()==0){
                System.out.println("No hay clientes.");
                this.mostrarMenuAdmi();
            }else{
                for (Cliente cliente: clientes){
                    System.out.println(cliente.toString());
                }
                this.mostrarMenuAdmi();
            }
        break;
        }
            
        case "1.5":
        {
            this.mostrarMenuPrincipal();
            break;
        }
        default:
            System.out.println("Opcion invalida.");
            this.mostrarMenuAdmi();
    } 
}


private void mostrarMenuCliente(Cliente cliente){
    System.out.println("-----Menu de transacciones-----");
    System.out.println("2.1 Ver informacion de cuentas.");
    System.out.println("2.2 Retiro de efectivo.");
    System.out.println("2.3 Deposito de efectivo.");
    System.out.println("2.4 Regresar.");
    System.out.print("Seleccione una opcion: ");
    dato = entrada.next();
    switch(dato){
        case "2.1":
        {
            if(cliente.getCantidadCuentas()==0){
                System.out.println("No hay cuentas asociadas con el cliente");
                this.mostrarMenuCliente(cliente);
            }else{
                System.out.println(cliente.toString());
            }
        break;
        }
        
        case "2.2":
        {
            System.out.println("Estan son sus cuentas");
            System.out.println(cliente.toString());
            System.out.println("----------");
            System.out.print("Ingrese el numero de cuenta: ");
            int posicion = -1;
            try{
                int numero = Integer.parseInt(entrada.next());
                for (int i = 0;i < cliente.getCantidadCuentas();i++){
                    if(cliente.getCuenta(i).getNumero()==numero){
                        posicion = i;
                        break;
                    }
                }
                if (posicion == -1){
                    System.out.println("No se encontro una cuenta con ese numero.");
                    this.mostrarMenuCliente(cliente);
                }else{
                    System.out.print("Ingrese el monto a retirar: ");
                    try{
                        double monto = Double.parseDouble(entrada.next());
                        if (monto < 0){
                            monto = monto * -1;
                        }else{
                            try{
                                cliente.getCuenta(posicion).retirar(monto);
                                System.out.println("El retiro fue efectuado con exito.");
                                this.mostrarMenuCliente(cliente);
                            }catch(Exception e){
                                System.out.println("No se puede efectuar la operacion.");
                                this.mostrarMenuCliente(cliente);
                            }
                        }
                    }catch(NumberFormatException e){
                        System.out.println("Debe ingresar un monto numerico");
                        this.mostrarMenuCliente(cliente);
                    } 
                }  
            }catch(NumberFormatException i){
                System.out.println("No hay ninguna cuenta asociada con ese numero");
                this.mostrarMenuCliente(cliente);
            }
        break;
        }
        
        case "2.3":
        {
            System.out.println("Estan son sus cuentas");
            int cant = cliente.getCantidadCuentas();
            if (cant == 0){
                System.out.println("No hay cuentas asociadas con el cliente");
                this.mostrarMenuCliente(cliente);
            }else{
                System.out.print("Ingrese el numero de cuenta: ");
            int posicion = -1;
            try{
                int numero = Integer.parseInt(entrada.next());
                for (int i=0;i < cliente.getCantidadCuentas();i++){
                    if(cliente.getCuenta(i).getNumero()==numero){
                        posicion = i;
                        break;
                    }
                }
                if (posicion == -1){
                    System.out.println("No se encontro una cuenta con ese numero.");
                    this.mostrarMenuCliente(cliente);
                }else{
                    System.out.print("Ingrese el monto a retirar: ");
                    try{
                        double monto = Double.parseDouble(dato);
                        if(monto > 0){
                            cliente.getCuenta(posicion).depositar(monto);
                        }else{
                            System.out.println("Debe depositar un monto numerico positivo");
                            this.mostrarMenuCliente(cliente);
                        }
                    }catch(NumberFormatException e){
                        System.out.println("Debe ingresar un monto numerico");
                        this.mostrarMenuCliente(cliente);
                    }   
                }  
            }catch(Exception e){
                System.out.println("No hay ninguna cuenta asociada con ese numero");
                this.mostrarMenuCliente(cliente);
            }
            }
        break;
        }
        
        case "2.4":
        {
            this.mostrarMenuPrincipal();
            break;
        }
        
        default:
        {
            this.mostrarMenuCliente(cliente);
        }
    }
}
    //metodo de salir de menu
private void salir(){
    System.exit(0);
}
    
}
