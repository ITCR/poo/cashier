/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cajero;
import java.util.ArrayList;

/**
 *
 * @author josemurillo
 */
public class Cliente {
    private String nombre;
    private String cedula;
    private String password;
    private static final int MAX_CUENTAS = 5;
    private ArrayList<Cuenta> cuentas = new ArrayList();
   
//constructtor
public Cliente(String nombre, String cedula, String password){
    this.nombre = nombre;
    this.cedula = cedula;
    this.password = password;
}

public void setNombre(String nom){
    this.nombre = nom;
}

public void setCedula(String ced){
    this.cedula = ced;
}

public void setPassword(String pass){
    this.password = pass;
}

public boolean valiDarPassword(String pass){
    return (this.password.equalsIgnoreCase(pass));
}

//metodo validar datos con los de cliente
boolean validar(String pass, String ced){
    return (this.password.equalsIgnoreCase(pass) && this.cedula.equalsIgnoreCase(ced));
}

//metodo get de nombre
public String getNombre(){
    return this.nombre;
}
    
//metodo get de cedula
public String getCedula(){
    return this.cedula;
}  

//metodo crearCuenta
public void crearCuenta(double montoInicial) throws Exception{
    if (this.cuentas.size()==5){
        throw new Exception("No se puede crear mas cuentas para este cliente.");
    }else if(montoInicial < 20000.0){
        throw new Exception("Necesita un monto incial de 20,000 para crear la cuenta.");
    }else{
    Cuenta c1 = new Cuenta(montoInicial);
    cuentas.add(c1);
    }
}

//metodo get de CantidadCuentas
public int getCantidadCuentas(){
    return this.cuentas.size();
}

//metodo get de cuenta en posicion
public Cuenta getCuenta(int posicion){
    return this.cuentas.get(posicion);
}

//metodo toString

    @Override
    public String toString(){
    String resultado = "";
    resultado = resultado + "Nombre: " + this.getNombre() + 
    "\n Cedula: " + this.getCedula() + "\n Cuentas: \n";
        for (Cuenta cuenta : this.cuentas) {
            resultado = resultado + cuenta.toString()+"\n";
        }
        return resultado;
    }

    




}
