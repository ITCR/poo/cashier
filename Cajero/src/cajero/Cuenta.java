/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cajero;

/**
 *
 * @author josemurillo
 */
public class Cuenta {
  private final int numero;
  private double saldo;
  private static int consecutivo;
  private static final double MONTO_MIN = 20000;
  
public Cuenta(double montoInicial){
    this.numero = consecutivo;
    consecutivo++;
    this.saldo = montoInicial;
    }

//metodo get de numero de cuenta
public int getNumero(){
    return this.numero;
}

//metodo get de saldo
public double getSaldo(){
    return this.saldo;
}

//metodo para depositar dinero
public void depositar(double monto){
    this.saldo = this.saldo + monto;
    System.out.println("El monto: " + monto + " ha sido depositado.");
}

//metodo para retirar dinero
public void retirar(double monto) throws Exception {
    if (this.saldo < monto){
        throw new Exception("Saldo es menor a cantidad que desea retirar.");
    }
    else if(this.saldo == 0){
        throw new Exception("No tiene saldo.");
    }else if (monto < 0){
        throw new Exception("Cantidad a retirar debe ser un numero positivo");
    }else{
        this.saldo = this.saldo - monto;
    }
}

//meotdo que retorna String con datos sobre cuenta
@Override
public String toString(){
    return "Cuenta numero: " + this.numero + "\n Saldo: " + this.saldo;
}

}

